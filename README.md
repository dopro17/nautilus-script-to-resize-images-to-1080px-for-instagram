# Nautilus script to resize images to 1080px for instagram



## To install the script

Just copy the file to your Nautilus script folder "~/.local/share/nautilus/scripts" The menu will appear in Right-mouse > Scripts.
```console
foo@bar:~$ cp Resize\ to\ 1080 ~/.local/share/nautilus/scripts/Resize\ to\ 1080
```

And don't forget to make it executable:
```console
foo@bar:~/.local/share/nautilus/scripts$ chmod +x Resize\ to\ 1080
```
